package embyapi

import (
	"bufio"
	"bytes"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/google/uuid"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

const traceHeader = "X-EmbySweeper-Trace-ID"

func newHttpTransport(logDir string, log wlog.Logger) httpTransport { // TODO accept a flag that will disable scrubbing
	return httpTransport{
		dir: logDir,
		log: log,
	}
}

type httpTransport struct {
	dir      string
	id       string
	fileName string
	log      wlog.Logger
}

func (ht httpTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	ht.id = uuid.New().String()
	r.Header.Add(traceHeader, ht.id)
	ht.fileName = filepath.Clean(fmt.Sprintf("%s/%s.log", ht.dir, ht.id))
	log := ht.log.WithScope(wlog.Fields{"url": r.URL.String(), "id": ht.id})
	log.Debug("http req")

	reqBytes, reqErr := httputil.DumpRequestOut(r, true)

	resp, err := http.DefaultTransport.RoundTrip(r) // err is returned after dumping the response

	var respBytes []byte
	var respErr error
	if resp != nil {
		resp.Header.Add(traceHeader, ht.id)
		respBytes, respErr = httputil.DumpResponse(resp, true)
	}

	func() {
		defer func() {
			if err := recover(); err != nil {
				log.Errorf("estransport: %v", err)
			}
		}()

		lo.Must0(reqErr)
		lo.Must0(respErr)
		if len(reqBytes) == 0 && len(respBytes) == 0 {
			return
		}

		f := lo.Must(os.OpenFile(ht.fileName, os.O_CREATE|os.O_RDWR, 0600))
		defer f.Close()

		if len(reqBytes) > 0 {
			lo.Must(f.WriteString("===REQUEST===\n"))
			lo.Must(f.Write(scrubApiKey(reqBytes, false)))
			lo.Must(f.WriteString("\n"))
		}

		if len(respBytes) > 0 {
			lo.Must(f.WriteString("===RESPONSE===\n"))
			lo.Must(f.Write(scrubToken(respBytes, false)))
			lo.Must(f.WriteString("\n"))
		}
	}()

	return resp, err
}

var apiKeyRegex = regexp.MustCompile(`(?:^X-Emby-Token: (\w+))|(?:"Pw":"(.+?)(?:"[,}]))`)

func scrubApiKey(in []byte, skip bool) []byte {
	if skip {
		return in
	}

	found := false
	sb := strings.Builder{}
	s := bufio.NewScanner(bytes.NewReader(in))
	for s.Scan() {
		l := s.Text()
		if !found {
			if m := apiKeyRegex.FindStringSubmatch(l); m != nil {
				found = true
				lo.Must(sb.WriteString(strings.Replace(l, lo.Ternary(m[1] == "", m[2], m[1]), "***", 1)))
			} else {
				lo.Must(sb.WriteString(l))
			}
		} else {
			lo.Must(sb.WriteString(l))
		}
		lo.Must(sb.WriteString("\n"))
	}
	return []byte(sb.String())
}

var tokenRegex = regexp.MustCompile(`"AccessToken":"(\w+?)(?:"[,}])`)

func scrubToken(in []byte, skip bool) []byte {
	if skip {
		return in
	}

	found := false
	sb := strings.Builder{}
	s := bufio.NewScanner(bytes.NewReader(in))
	for s.Scan() {
		l := s.Text()
		if !found {
			if m := tokenRegex.FindStringSubmatch(l); m != nil {
				found = true
				lo.Must(sb.WriteString(strings.Replace(l, m[1], "***", 1)))
			} else {
				lo.Must(sb.WriteString(l))
			}
		} else {
			lo.Must(sb.WriteString(l))
		}
		lo.Must(sb.WriteString("\n"))
	}
	return []byte(sb.String())
}
