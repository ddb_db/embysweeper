package embyapi

import (
	"context"
	"fmt"
	"net/http"
	"slices"
	"strings"
	"time"

	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

type UserID string

const unknown = "[unknown]"

type ESClientWithResponsesInterface interface {
	ClientWithResponsesInterface
	DeleteItem(item BaseItemDto, forReal bool, userCtx UserID)
	DescribeItem(item BaseItemDto, userID UserID) string
	FilterItemsWithParentID(parentID string, userID UserID, filters map[string]string, fields ...string) []BaseItemDto
	GetFilteredViews(userID UserID, included, excluded []string) []string
	GetLibraryID(name string) string
	GetSeriesInLibrary(libName string, userID UserID, filters map[string]string, fields ...string) []BaseItemDto
	GetUserID(user string) (UserID, bool)
	IsItemSeriesMarkedFav(userID UserID, item BaseItemDto) bool
	IsItemSeriesTagged(tag string, item BaseItemDto) bool
	IsItemWatched(data UserItemDataDto, minDays int) bool
}

type ESClientWithResponses struct {
	*ClientWithResponses
	log wlog.Logger
}

func (clnt *ESClientWithResponses) GetSeriesInLibrary(libName string, userID UserID, filters map[string]string, fields ...string) []BaseItemDto {
	f := lo.Assign(filters, map[string]string{"IncludeItemTypes": "Series"})
	pid := clnt.GetLibraryID(libName)
	return clnt.FilterItemsWithParentID(pid, userID, f, fields...)
}

func (clnt *ESClientWithResponses) FilterItemsWithParentID(parentID string, userID UserID, filters map[string]string, fields ...string) []BaseItemDto {
	var statusCode int
	var status string
	var qryResult *QueryResultBaseItemDto
	if userID == "" {
		params := &GetItemsParams{
			ParentId:  lo.ToPtr(parentID),
			Recursive: lo.ToPtr(true),
		}
		if len(fields) > 0 {
			params.Fields = lo.ToPtr(strings.Join(fields, ","))
		}

		items, err := clnt.GetItemsWithResponse(context.TODO(), params, clnt.applyFiltersFn(filters))
		lo.Must(items, err, "embyapi: GetItems errored")
		statusCode = items.StatusCode()
		status = items.Status()
		qryResult = items.JSON200
	} else {
		params := &GetUsersByUseridItemsParams{
			ParentId:  lo.ToPtr(parentID),
			Recursive: lo.ToPtr(true),
		}
		if len(fields) > 0 {
			params.Fields = lo.ToPtr(strings.Join(fields, ","))
		}

		items, err := clnt.GetUsersByUseridItemsWithResponse(context.TODO(), string(userID), params, clnt.applyFiltersFn(filters))
		lo.Must(items, err, "embyapi: GetUserItems errored")
		statusCode = items.StatusCode()
		status = items.Status()
		qryResult = items.JSON200
	}
	lo.Must0(statusCode == http.StatusOK, "embyapi: FilterLibraryItems failed: %s", status)
	return *qryResult.Items
}

func (clnt *ESClientWithResponses) applyFiltersFn(filters map[string]string) RequestEditorFn {
	return func(ctx context.Context, req *http.Request) error {
		if len(filters) == 0 {
			return nil
		}
		params := req.URL.Query()
		for k, v := range filters {
			params.Set(k, v)
		}
		req.URL.RawQuery = params.Encode()
		clnt.log.Debugf("items filter: %+v", req.URL.Query())
		return nil
	}
}

func (clnt *ESClientWithResponses) DeleteItem(item BaseItemDto, forReal bool, userCtx UserID) {
	msg := "SKIPPED (-d not present)"
	if forReal {
		resp, err := clnt.PostItemsDeleteWithResponse(context.TODO(), &PostItemsDeleteParams{Ids: *item.Id})
		if err != nil {
			msg = fmt.Sprintf("ERROR: %s", err.Error())
		} else if resp.StatusCode() != http.StatusNoContent {
			msg = fmt.Sprintf("ERROR: %s", resp.Status())
		} else {
			msg = "DELETED"
		}
	}
	clnt.log.Warningf("found: %s\n\tResult: %s", clnt.DescribeItem(item, userCtx), msg)
}

func (clnt *ESClientWithResponses) GetUserID(user string) (UserID, bool) {
	users, err := clnt.GetUsersQueryWithResponse(context.TODO(), nil)
	lo.Must(users, err, "embyapi")
	p, ok := lo.Find(*users.JSON200.Items, func(u UserDto) bool { return u.Name != nil && *u.Name == user })
	if ok {
		return UserID(*p.Id), ok
	}
	return "", ok
}

func (clnt *ESClientWithResponses) GetFilteredViews(userID UserID, included, excluded []string) []string {
	var views []string
	if userID != "" {
		views = getUserFilteredViews(clnt, userID, included, excluded)
	} else {
		views = getFilteredViews(clnt, included, excluded)
	}
	return views
}

func getUserFilteredViews(api ClientWithResponsesInterface, userID UserID, included, excluded []string) []string {
	resp, err := api.GetUsersByUseridViewsWithResponse(context.TODO(), string(userID), nil)
	lo.Must(resp, err, "embyapi")
	lo.Must0(resp.StatusCode() == http.StatusOK, "embyapi: GetUserViews failed: %s", resp.Status())
	items := *resp.JSON200.Items
	for _, l := range items {
		libIDs[*l.Name] = *l.Guid
	}
	return ApplyIncludeExcludeFilters(lo.Map(items, func(item BaseItemDto, idx int) string { return *item.Name }), included, excluded)
}

func getFilteredViews(api ClientWithResponsesInterface, included, excluded []string) []string {
	resp, err := api.GetLibraryMediafoldersWithResponse(context.TODO(), nil)
	lo.Must(resp, err, "embyapi")
	lo.Must0(resp.StatusCode() == http.StatusOK, "embyapi: GetLibraryFolders failed: %s", resp.Status())
	items := *resp.JSON200.Items
	for _, l := range items {
		libIDs[*l.Name] = *l.Guid
	}
	return ApplyIncludeExcludeFilters(lo.Map(items, func(item BaseItemDto, idx int) string { return *item.Name }), included, excluded)
}

func ApplyIncludeExcludeFilters(input, included, excluded []string) []string {
	if input == nil {
		return []string{}
	}

	result := slices.Clone(input)
	if len(included) > 0 {
		result = lo.Intersect(result, included)
	}
	if len(excluded) > 0 {
		result = lo.Without(result, excluded...)
	}
	return result
}

func (clnt *ESClientWithResponses) IsItemWatched(data UserItemDataDto, minDays int) bool {
	return data.Played != nil &&
		*data.Played &&
		data.LastPlayedDate != nil &&
		lo.IsNotEmpty(*data.LastPlayedDate) &&
		time.Now().Add(time.Duration(-24*time.Hour*time.Duration(minDays))).After(*data.LastPlayedDate)
}

func (clnt *ESClientWithResponses) DescribeItem(item BaseItemDto, userID UserID) string {
	sb := strings.Builder{}
	lo.Must(sb.WriteString(getItemName(item)))

	lastPlayed := time.Time{}
	if item.UserData != nil && item.UserData.LastPlayedDate != nil {
		lastPlayed = *item.UserData.LastPlayedDate
	}

	lo.Must(sb.WriteString(fmt.Sprintf("\n\tPath: %s\n\tCreated: %s\n\tPremiered: %s\n\tIsSeriesFav: %t\n\tLastWatched: %s",
		lo.FromPtrOr(item.Path, unknown),
		lo.FromPtrOr(item.DateCreated, time.Time{}),
		lo.FromPtrOr(item.PremiereDate, time.Time{}),
		clnt.IsItemSeriesMarkedFav(userID, item),
		lastPlayed)),
	)
	return sb.String()
}

func getItemName(item BaseItemDto) string {
	sb := strings.Builder{}
	if item.SeriesName != nil && len(*item.SeriesName) > 0 {
		lo.Must(sb.WriteString(*item.SeriesName + ": "))
	}
	lo.Must(sb.WriteString(lo.FromPtrOr(item.Name, unknown)))
	return sb.String()
}

var libIDs = map[string]string{}

func (clnt *ESClientWithResponses) GetLibraryID(name string) string {
	id, ok := libIDs[name]
	return lo.Must(id, ok, "embyapi: unknown library: %s", name)
}

var tagCache map[string]bool = map[string]bool{}

func (clnt *ESClientWithResponses) IsItemSeriesTagged(tag string, item BaseItemDto) bool {
	id := lo.FromPtr(item.SeriesId)
	if id == "" {
		return false
	}

	key := fmt.Sprintf("%s:%s", id, tag)
	status, ok := tagCache[key]
	if ok {
		return status
	}

	resp, err := clnt.GetItemsWithResponse(context.TODO(), &GetItemsParams{Ids: &id, Fields: lo.ToPtr("Tags")})
	lo.Must(resp, err, "embyapi: series lookup failed")
	lo.Must0(*resp.JSON200.TotalRecordCount == 1, "embyapi: series not found")
	val := (*resp.JSON200.Items)[0]
	tagCache[key] = lo.ContainsBy(*val.TagItems, func(t NameLongIdPair) bool { return *t.Name == tag })
	return tagCache[key]
}

var seriesCache map[string]bool = map[string]bool{}

func (clnt *ESClientWithResponses) IsItemSeriesMarkedFav(userID UserID, item BaseItemDto) bool {
	if userID == "" {
		return false
	}

	id := lo.FromPtr(item.SeriesId)
	if id == "" {
		return false
	}

	status, ok := seriesCache[id]
	if ok {
		return status
	}

	resp, err := clnt.GetUsersByUseridItemsWithResponse(context.TODO(), string(userID), &GetUsersByUseridItemsParams{Ids: &id, Fields: lo.ToPtr("UserDataIsFavorite")})
	lo.Must(resp, err, "embyapi: series lookup failed")
	lo.Must0(*resp.JSON200.TotalRecordCount == 1, "embyapi: series not found")
	val := (*resp.JSON200.Items)[0]
	seriesCache[id] = *val.UserData.IsFavorite
	return *val.UserData.IsFavorite
}
