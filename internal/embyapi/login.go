package embyapi

import (
	"context"
	"fmt"
	"net/http"

	"github.com/deepmap/oapi-codegen/pkg/securityprovider"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

func Login(server, username, password, appVer string, log wlog.Logger, httpLogDir string) *ESClientWithResponses {
	clntOpts := make([]ClientOption, 0, 1)
	if httpLogDir != "" && log.GetLogLevel() == wlog.Dbg {
		httpClnt := &http.Client{
			Transport: newHttpTransport(httpLogDir, log),
		}
		clntOpts = append(clntOpts, WithHTTPClient(httpClnt))
		log.Debugf("http logging enabled: %s", httpLogDir)
	}

	api, err := NewClientWithResponses(server, clntOpts...)
	lo.Must(api, err, "embyapi")
	resp, err := api.PostUsersAuthenticatebynameWithResponse(context.TODO(), &PostUsersAuthenticatebynameParams{
		XEmbyAuthorization: fmt.Sprintf("Emby UserID=\"%s\", Client=\"EmbySweeper\", Device=\"EmbySweeper\", DeviceId=\"EmbySweeper\", Version=\"%s\"", username, appVer),
	}, PostUsersAuthenticatebynameJSONRequestBody{
		Username: &username,
		Pw:       &password,
	})
	lo.Must(resp, err, "embyapi")

	switch resp.StatusCode() {
	case http.StatusOK:
		apikeyProvider, err := securityprovider.NewSecurityProviderApiKey("header", "X-Emby-Token", *resp.JSON200.AccessToken)
		lo.Must(apikeyProvider, err, "embyapi")
		clntOpts = append(clntOpts, WithRequestEditorFn(apikeyProvider.Intercept))

		api, err := NewClientWithResponses(server, clntOpts...)
		lo.Must(api, err, "embyapi")
		return &ESClientWithResponses{
			ClientWithResponses: api,
			log:                 log,
		}
	default:
		panic(fmt.Errorf("embyapi: auth error: %s", resp.Status()))
	}
}
