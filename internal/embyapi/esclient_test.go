package embyapi_test

import (
	"testing"
	"time"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/embysweeper/internal/embyapi"
)

func TestApplyIncludeExcludeFiltersBehavesAsExpected(t *testing.T) {
	tests := []struct {
		input    []string
		incl     []string
		excl     []string
		expected []string
		desc     string
	}{
		{nil, []string{}, []string{}, []string{}, "nil input returns empty slice"},
		{[]string{"a"}, nil, []string{}, []string{"a"}, "nil incl & nil excl returns input"},
		{[]string{"a"}, []string{}, nil, []string{"a"}, "empty incl returns input"},
		{[]string{"a", "b", "c"}, []string{"b"}, nil, []string{"b"}, "incl filter applied"},
		{[]string{"a", "b", "c"}, nil, []string{"b"}, []string{"a", "c"}, "excl filter applied"},
		{[]string{"a", "b", "c"}, []string{"a", "b"}, []string{"b"}, []string{"a"}, "incl & excl filter applied"},
		{[]string{"a", "b", "c"}, []string{"a", "b", "c"}, []string{"a", "b", "c"}, []string{}, "same incl & excl returns empty slice"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			result := embyapi.ApplyIncludeExcludeFilters(tc.input, tc.incl, tc.excl)
			assert.Equal(t, tc.expected, result)
		})
	}
}

func TestIsItemWatchedBehavesAsExpected(t *testing.T) {
	tests := []struct {
		played         *bool
		lastPlayedDate *time.Time
		minWatchedDays int
		expected       bool
		desc           string
	}{
		{nil, nil, 0, false, "nothing available"},
		{nil, lo.ToPtr(time.Now().Add(-24 * time.Hour)), 0, false, "false when played is nil"},
		{lo.ToPtr(false), lo.ToPtr(time.Now().Add(-24 * time.Hour)), 0, false, "false when played is false"},
		{lo.ToPtr(true), nil, 0, false, "false when last watched is nil"},
		{lo.ToPtr(true), lo.ToPtr(time.Now()), 1, false, "false when last watched is too recent"},
		{lo.ToPtr(true), lo.ToPtr(time.Now().Add(-24 * time.Hour)), 0, true, "true"},
		{lo.ToPtr(true), lo.ToPtr(time.Now().Add(-24*time.Hour - time.Second)), 1, true, "true by a second"},
		{lo.ToPtr(true), lo.ToPtr(time.Now().Add(-24*3600*time.Second + time.Second)), 1, false, "false by a second"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			sut := defaultESClient()
			result := sut.IsItemWatched(embyapi.UserItemDataDto{
				Played:         tc.played,
				LastPlayedDate: tc.lastPlayedDate,
			}, tc.minWatchedDays)
			assert.Equal(t, tc.expected, result)
		})
	}
}

func defaultESClient() *embyapi.ESClientWithResponses {
	return &embyapi.ESClientWithResponses{
		ClientWithResponses: nil,
	}
}
