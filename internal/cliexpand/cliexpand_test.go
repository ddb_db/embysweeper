package cliexpand_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/embysweeper/internal/cliexpand"
)

func TestMultiLineProcessing(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"@testdata/multi_line.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{"a", "b", "c", "d e   f g", "h", "i"}, toks)
}

func TestEmptyArgDoesntAttemptFileExpansion(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"@testdata/empty_string.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{""}, toks)
}

func TestEscapedAtPassesThru(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"x", "@@testdata/foo.txt", "@testdata/escaped_at.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{"x", "@testdata/foo.txt", "@", "@", "@foo.txt", "@@foo.txt", "@@@foo.txt"}, toks)
}

func TestSingleIncludeFile(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"@testdata/single_base.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{"a", "0", "1", "2"}, toks)
}

func TestMultiIncludeFiles(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"@testdata/multi_base.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{"x", "1", "2", "3 4", "y z", "9", "8", "7", "@testdata/multi_inc3.txt"}, toks)
}

func TestDeepIncludeFiles(t *testing.T) {
	toks, err := cliexpand.Expand([]string{"@testdata/deep_base.txt"})
	assert.NoError(t, err)
	assert.Equal(t, []string{"2", "3", "", "a", "foo", "bar", "zoo", "4", "5", "0", "1"}, toks)
}

func TestInvalidFileReturnsError(t *testing.T) {
	_, err := cliexpand.Expand([]string{"@testdata/invalid_file.txt"})
	assert.ErrorContains(t, err, "cliexpand: open invalid.txt:")
}

func TestCycleDetected(t *testing.T) {
	_, err := cliexpand.Expand([]string{"@testdata/cycle.txt"})
	assert.EqualError(t, err, "cliexpand: reference cycle [testdata/cycle.txt]")

	_, err = cliexpand.Expand([]string{"@testdata/cycle_inc.txt"})
	assert.EqualError(t, err, "cliexpand: reference cycle [testdata/cycle_inc.txt]")
}
