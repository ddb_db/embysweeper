package cliexpand

import (
	"bufio"
	"fmt"
	"os"

	"github.com/google/shlex"
)

func Expand(args []string) ([]string, error) {
	result := make([]string, 0)
	for _, a := range args {
		if len(a) > 1 && a[0] == '@' {
			if a[1] == '@' {
				result = append(result, a[1:])
				continue
			}
			f, err := os.Open(a[1:])
			if err != nil {
				return nil, fmt.Errorf("cliexpand: %w", err)
			}
			defer f.Close()
			toks, err := expand(f, map[string]struct{}{})
			if err != nil {
				return nil, fmt.Errorf("cliexpand: %w", err)
			}
			result = append(result, toks...)
		} else {
			result = append(result, a)
		}
	}
	return result, nil
}

func expand(f *os.File, visited map[string]struct{}) ([]string, error) {
	if _, ok := visited[f.Name()]; ok {
		return nil, fmt.Errorf("reference cycle [%s]", f.Name())
	}
	visited[f.Name()] = struct{}{}

	result := make([]string, 0)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		tokens, err := shlex.Split(line)
		if err != nil {
			return nil, err
		}
		for _, t := range tokens {
			if len(t) > 1 && t[0] == '@' {
				if t[1] == '@' { // escape an @ arg with a double @
					result = append(result, t[1:])
					continue
				}

				f, err := os.Open(t[1:])
				if err != nil {
					return nil, err
				}
				defer f.Close()
				toks, err := expand(f, visited)
				if err != nil {
					return nil, err
				}
				result = append(result, toks...)
			} else {
				result = append(result, t)
			}
		}
	}
	return result, scanner.Err()
}
