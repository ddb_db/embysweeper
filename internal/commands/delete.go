package commands

import (
	"strings"

	"github.com/alecthomas/kong"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/embysweeper/internal/embyapi"
)

const requiredItemFields = "Path,UserDataLastPlayedDate"

type watchedDaysInt int

func (i watchedDaysInt) AfterApply(ctx *kong.Context) error {
	return checkFlagSet(ctx, "cleanup-user", "min-watched-days")
}

type isFavBool bool

func (b isFavBool) AfterApply(ctx *kong.Context) error {
	if !b {
		return nil
	}
	return checkFlagSet(ctx, "cleanup-user", "ignore-series-fav")
}

type Delete struct {
	Action
	//Audit           string `type:"path" xor:"cdaction" short:"a"`
	Delete          bool            `xor:"cdaction" short:"d" help:"items are only deleted if this option is set"`
	IgnoreSeriesFav isFavBool       `default:"false" help:"when set, items whose series is marked as a favorite are not deleted"`
	IgnoreSeriesTag string          `help:"when set, items whose series contain this tag are not deleted"`
	MinWatchedDays  *watchedDaysInt `help:"when set, items that were not watched at least this many days ago are not deleted"`

	api           embyapi.ESClientWithResponsesInterface `kong:"-"`
	cleanupUserID embyapi.UserID
}

func (cmd Delete) Run(api embyapi.ESClientWithResponsesInterface, cleanupUserID embyapi.UserID, ctx *kong.Context, log wlog.Logger) error {
	cmd.api = api
	cmd.cleanupUserID = cleanupUserID

	for _, l := range api.GetFilteredViews(cleanupUserID, cmd.LibInclude, cmd.LibExclude) {
		log.Infof("Cleaning up library: %s", l)
		pid := api.GetLibraryID(l)
		for _, i := range cmd.applyCommandFilters(cmd.api.FilterItemsWithParentID(pid, cmd.cleanupUserID, cmd.Filters, strings.Split(requiredItemFields, ",")...)) {
			cmd.api.DeleteItem(i, cmd.Delete, cmd.cleanupUserID)
		}
	}
	return nil
}

func (cmd Delete) applyCommandFilters(items []embyapi.BaseItemDto) []embyapi.BaseItemDto {
	return lo.Filter(items, func(item embyapi.BaseItemDto, idx int) bool {
		return (!bool(cmd.IgnoreSeriesFav) || !cmd.api.IsItemSeriesMarkedFav(cmd.cleanupUserID, item)) &&
			(cmd.IgnoreSeriesTag == "" || !cmd.api.IsItemSeriesTagged(cmd.IgnoreSeriesTag, item)) &&
			(cmd.MinWatchedDays == nil || cmd.api.IsItemWatched(*item.UserData, int(*cmd.MinWatchedDays)))
	})
}
