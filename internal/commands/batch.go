package commands

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/alecthomas/kong"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/embysweeper/internal/cliexpand"
)

type Batch struct {
	FileExt string `name:"ext" default:"args" help:"files with this extension found in the given dir are executed"`
	DirName string `arg:"" type:"existingdir" help:"a directory containing one or more files to execute"`
	log     wlog.Logger
}

func (cmd Batch) Run(ctx *kong.Context, log wlog.Logger) error {
	cmd.log = log

	root := os.DirFS(cmd.DirName)
	files := lo.Must(fs.Glob(root, fmt.Sprintf("*.%s", cmd.FileExt)))
	failed := false
	for _, f := range files {
		path := filepath.Clean(fmt.Sprintf("%s/%s", cmd.DirName, f))
		log.Infof(">>> Running batch file: %s", path)
		cmdline := fmt.Sprintf("@%s", path)
		func() {
			defer func() {
				if err := recover(); err != nil {
					failed = true
					log.Errorf("batch: file '%s' failed: %v", f, err)
				}
			}()

			termFn := cmd.terminateBatch(path)
			Start(lo.Must(cliexpand.Expand([]string{cmdline})), AppVersion, &termFn)
		}()
		log.Infof("<<< End Batch File")
	}
	return lo.Ternary(failed, errors.New("one or more batch files failed"), nil)
}

func (cmd Batch) terminateBatch(path string) func(int) {
	return func(rc int) {
		cmd.log.Warningf("batch: %s: terminated with exit code %d", path, rc)
	}
}
