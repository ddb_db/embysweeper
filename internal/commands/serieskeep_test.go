package commands_test

import (
	"fmt"
	"net/http"
	"net/url"
	"testing"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/embysweeper/internal/commands"
)

func TestSeriesKeepDryRunByDefault(t *testing.T) {
	resps := []EmbyApiResponse{
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Library/MediaFolders"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
	}
	srv := newServer(t, resps)
	defer srv.CloseAndVerify()
	url := lo.Must(url.Parse(srv.URL))

	args := []string{"serieskeep", "--server-host", url.Hostname(), "--server-port", url.Port(), "--server-login=foo", "--server-password=bar", "--keep=0"}
	assert.NotPanics(t, func() { commands.Start(args, "0.0", nil) })
}

func TestSeriesKeepCallDeleteApi(t *testing.T) {
	resps := []EmbyApiResponse{
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Library/MediaFolders"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
		{nil, http.StatusNoContent, "/Items/Delete"},
	}
	srv := newServer(t, resps)
	defer srv.CloseAndVerify()
	url := lo.Must(url.Parse(srv.URL))

	args := []string{"serieskeep", "--server-host", url.Hostname(), "--server-port", url.Port(), "--server-login=foo", "--server-password=bar", "--keep=0", "-d"}
	assert.NotPanics(t, func() { commands.Start(args, "0.0", nil) })
}

func TestSeriesKeepFailsWhenUserContextIsMissingForSeriesFilters(t *testing.T) {
	tests := []string{
		"IsPlayed=true",
		"IsFavorite=false",
	}

	for _, tc := range tests {
		t.Run(tc, func(t *testing.T) {
			args := []string{"serieskeep", "--server-login=foo", "--server-password=bar", fmt.Sprintf("--series-filters=%s", tc), "--keep=3"}
			var pErr string
			func() {
				defer func() {
					if err := recover(); err != nil {
						pErr = fmt.Sprintf("%v", err)
					}
				}()
				commands.Start(args, "0.0", nil)
			}()
			assert.Contains(t, pErr, "filter requires --cleanup-user")
		})
	}
}
