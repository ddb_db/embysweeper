package commands_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/embysweeper/internal/commands"
	"gitlab.com/ddb_db/embysweeper/internal/embyapi"
)

type EmbyApiResponse struct {
	Payload          any
	StatusCode       int
	ExpectedEndpoint string
}

func TestDeleteDryRunByDefault(t *testing.T) {
	resps := []EmbyApiResponse{
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Library/MediaFolders"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
	}
	srv := newServer(t, resps)
	defer srv.CloseAndVerify()
	url := lo.Must(url.Parse(srv.URL))

	args := []string{"delete", "--server-host", url.Hostname(), "--server-port", url.Port(), "--server-login=foo", "--server-password=bar"}
	assert.NotPanics(t, func() { commands.Start(args, "0.0", nil) })
}

func TestDeleteCallDeleteApi(t *testing.T) {
	resps := []EmbyApiResponse{
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Library/MediaFolders"},
		{defaultQueryResultBaseItemDto(), http.StatusOK, "/Items"},
		{nil, http.StatusNoContent, "/Items/Delete"},
	}
	srv := newServer(t, resps)
	defer srv.CloseAndVerify()
	url := lo.Must(url.Parse(srv.URL))

	args := []string{"delete", "--server-host", url.Hostname(), "--server-port", url.Port(), "--server-login=foo", "--server-password=bar", "-d"}
	assert.NotPanics(t, func() { commands.Start(args, "0.0", nil) })
}

func TestDeleteFailsWhenUserContextIsMissingForFilters(t *testing.T) {
	tests := []string{
		"IsPlayed=true",
		"IsFavorite=false",
	}

	for _, tc := range tests {
		t.Run(tc, func(t *testing.T) {
			args := []string{"delete", "--server-login=foo", "--server-password=bar", fmt.Sprintf("--filters=%s", tc)}
			var pErr string
			func() {
				defer func() {
					if err := recover(); err != nil {
						pErr = fmt.Sprintf("%v", err)
					}
				}()
				commands.Start(args, "0.0", nil)
			}()
			assert.Contains(t, pErr, "filter requires --cleanup-user")
		})
	}
}

func defaultQueryResultBaseItemDto() embyapi.QueryResultBaseItemDto {
	return embyapi.QueryResultBaseItemDto{
		Items: lo.ToPtr([]embyapi.BaseItemDto{
			{Name: lo.ToPtr("Foobar"), Guid: lo.ToPtr("guid"), Id: lo.ToPtr("Id")},
		}),
		TotalRecordCount: lo.ToPtr(int32(1)),
	}
}

type assertServer struct {
	*httptest.Server
	resps []EmbyApiResponse
	t     *testing.T
}

func (srv *assertServer) CloseAndVerify() {
	defer srv.Close()
	assert.Empty(srv.t, srv.resps, "%d expected api calls not made", len(srv.resps))
}

func (srv *assertServer) Handler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.True(srv.t, len(srv.resps) > 0, "unexpected api call: %s", r.URL)
		resp := srv.resps[0]
		if len(srv.resps) > 1 {
			srv.resps = srv.resps[1:]
		} else {
			srv.resps = []EmbyApiResponse{}
		}
		if resp.ExpectedEndpoint != "" {
			assert.Equal(srv.t, resp.ExpectedEndpoint, r.URL.Path, "incorrect api call")
		}
		if resp.Payload != nil {
			w.Header().Set("Content-Type", "application/json")
		}
		w.WriteHeader(resp.StatusCode)
		if resp.Payload != nil {
			enc := lo.Must(json.Marshal(resp.Payload))
			lo.Must(w.Write(enc))
		}
	})
}

func newServer(t *testing.T, responses []EmbyApiResponse) *assertServer {
	// always login
	list := make([]EmbyApiResponse, 0, 1+len(responses))
	list = append(list, EmbyApiResponse{embyapi.AuthenticationAuthenticationResult{
		AccessToken: lo.ToPtr("foobar"),
	}, http.StatusOK, "/Users/AuthenticateByName"})
	list = append(list, responses...)

	asSrv := &assertServer{
		resps: list,
		t:     t,
	}

	srv := httptest.NewServer(asSrv.Handler())
	asSrv.Server = srv
	return asSrv
}
