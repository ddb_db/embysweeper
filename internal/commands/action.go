package commands

import (
	"fmt"
	"os"

	"github.com/alecthomas/kong"
	"github.com/samber/lo"
)

var AppVersion = "dev/unknown"

type Action struct {
	CleanupUser     string       `help:"Emby user to act as; some filters (IsPlayed, IsFavorite) require this user be set"`
	Filters         itemFilters  `help:"apply filters to find the items to delete; see https://dev.emby.media/reference/RestAPI/ItemsService/getItems.html"`
	IgnoreHttpsCert bool         `default:"false" help:"ignore ssl certificate errors when using https"`
	LibExclude      []string     `help:"list of library names to exclude from processing"`
	LibInclude      []string     `help:"list of library names to include for processing; if not specified then all libraries are included"`
	LogHttp         tmpDirString `type:"path" help:"when set, all Emby api calls are logged to the specified directory; when set to \"-\" then a new temp directory is created; requires debug logging"`
	ServerHost      string       `required:"" default:"localhost" help:"Emby server hostname"`
	ServerLogin     string       `required:"" help:"Emby server login; this user must have permissions to delete media"`
	ServerPassword  string       `required:"" help:"password for --server-login"`
	ServerPort      int          `required:"" default:"8096" help:"Emby server port"`
	UseHttps        bool         `default:"false" help:"connect to Emby server over https"`
}

type itemFilters map[string]string

func (filters itemFilters) AfterApply(ctx *kong.Context) error {
	for _, f := range []string{"IsPlayed", "IsFavorite"} {
		if _, ok := filters[f]; ok && checkFlagSet(ctx, "cleanup-user", f) != nil {
			return fmt.Errorf("embyapi: use of '%s' filter requires --cleanup-user", f)
		}
	}
	return nil
}

type tmpDirString string

func (name *tmpDirString) AfterApply() error {
	if *name == "-" {
		*name = tmpDirString(lo.Must(os.MkdirTemp("", "embysweeper_")))
	} else {
		lo.Must0(os.MkdirAll(string(*name), 0700))
	}
	return nil
}

func checkFlagSet(ctx *kong.Context, must, dep string) error {
	_, ok := lo.Find(ctx.Flags(), func(f *kong.Flag) bool { return f.Set && f.Name == must })
	if !ok {
		return fmt.Errorf("embyapi: --%s requires --%s", dep, must)
	}
	return nil
}
