package commands

import (
	"time"

	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/embysweeper/internal/embyapi"
)

type SeriesKeep struct {
	Action
	Delete        bool        `xor:"cdaction" short:"d" help:"items are only deleted if this option is set"`
	Keep          int         `required:"" help:"the maximum number of episodes to keep; older episodes are deleted until this number is reached"`
	MinAgeDays    *int        `help:"when set, items that were not created at least this many days ago are not deleted"`
	SeriesFilters itemFilters `help:"filters that are applied to the series search; use this to filter down series that are scanned"`

	api           embyapi.ESClientWithResponsesInterface `kong:"-"`
	cleanupUserID embyapi.UserID
}

func (cmd SeriesKeep) Run(api embyapi.ESClientWithResponsesInterface, cleanupUserID embyapi.UserID, log wlog.Logger) error {
	cmd.api = api
	cmd.cleanupUserID = cleanupUserID

	for _, l := range api.GetFilteredViews(cleanupUserID, cmd.LibInclude, cmd.LibExclude) {
		log.Infof("Scanning library: %s", l)
		series := api.GetSeriesInLibrary(l, cmd.cleanupUserID, cmd.SeriesFilters)
		for _, s := range series {
			log.Infof("Processing series: %s", *s.Name)
			filters := lo.Assign(map[string]string{
				"MediaTypes": "Video",
				"SortBy":     "PremiereDate,DateCreated",
				"SortOrder":  "Ascending",
			}, cmd.Filters)
			items := api.FilterItemsWithParentID(*s.Id, cmd.cleanupUserID, filters, "Path", "DateCreated", "PremiereDate")
			items = cmd.applyCommandFilters(items)
			log.Infof("found %d episodes", len(items))
			numToDel := len(items) - cmd.Keep
			if numToDel > 0 {
				log.Infof("deleting %d oldest episodes to comply with keep count request", numToDel)
				for i := 0; i < numToDel; i++ {
					api.DeleteItem(items[i], cmd.Delete, cmd.cleanupUserID)
				}
			} else {
				log.Infof("no more than --keep count episodes found, nothing to delete for this series")
			}
		}
	}
	return nil
}

func (cmd SeriesKeep) applyCommandFilters(items []embyapi.BaseItemDto) []embyapi.BaseItemDto {
	return lo.Filter(items, func(item embyapi.BaseItemDto, idx int) bool {
		return cmd.MinAgeDays == nil ||
			(item.DateCreated != nil && item.DateCreated.Before(time.Now().Add(-24*time.Duration(*cmd.MinAgeDays)*time.Hour)))
	})
}
