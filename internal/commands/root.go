package commands

import (
	"fmt"
	"os"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/embysweeper/internal/embyapi"
)

type Root struct {
	LogFile         string  `type:"path" help:"write log output to specified file; appends to existing file"`
	LogFileTruncate bool    `default:"false" name:"truncate-log" help:"when true, truncate the log file if it already exists"`
	Quiet           int     `type:"counter" short:"q" xor:"loglvl" help:"decrease log level; multiple allowed"`
	Verbose         int     `type:"counter" short:"v" xor:"loglvl" help:"increase log level; multiple allowed"`
	Version         verBool `help:"print version information and exit"`

	Batch      Batch      `cmd:"" help:"run all command lines found in the specified directory"`
	Delete     Delete     `cmd:"" help:"delete all matching items"`
	SeriesKeep SeriesKeep `cmd:"" name:"serieskeep" help:"scan all tv series applying specified filters and maintaining a maximum count of episodes, deleting the rest"`
}

type verBool bool

func (b verBool) BeforeReset(ctx *kong.Context) error {
	sb := strings.Builder{}
	sb.WriteString("EmbySweeper " + AppVersion)
	sb.WriteString("\nhttps://gitlab.com/ddb_db/embysweeper\n")
	lo.Must(ctx.Stdout.Write([]byte(sb.String())))
	ctx.Exit(1)
	return fmt.Errorf("halted on --version")
}

func Start(args []string, appVer string, fn *func(int)) {
	AppVersion = appVer
	root := &Root{}

	opts := []kong.Option{kong.Description("An Emby tool to sweep for and delete media items automatically based on a set of defined rules.\n\n\n@/path/to/file replaced by contents of file; see https://gitlab.com/ddb_db/embysweeper/-/wikis/Include-Files")}
	if fn != nil {
		opts = append(opts, kong.Exit(*fn))
	}

	cli := lo.Must(kong.New(root, opts...))
	ctx := lo.Must(cli.Parse(args))
	if fn != nil {
		ctx.Exit = *fn
	}
	log, file := mkLogger(root.Verbose-root.Quiet, root.LogFile, root.LogFileTruncate)
	if file != nil {
		defer file.Close()
	}
	ctx.BindTo(log, (*wlog.Logger)(nil))
	ctx.Bind(*root)

	var action *Action
	switch ctx.Selected().Name {
	case "delete":
		action = &root.Delete.Action
	case "serieskeep":
		action = &root.SeriesKeep.Action
	}
	login(action, ctx, log)

	ctx.FatalIfErrorf(ctx.Run())
}

func login(a *Action, ctx *kong.Context, log wlog.Logger) {
	if a == nil {
		return
	}

	url := fmt.Sprintf("%s://%s:%d", lo.Ternary(a.UseHttps, "https", "http"), a.ServerHost, a.ServerPort)
	log.Debugf("Emby server url: %s", url)
	api := embyapi.Login(url, a.ServerLogin, a.ServerPassword, AppVersion, log, string(a.LogHttp))
	ctx.BindTo(api, (*embyapi.ESClientWithResponsesInterface)(nil))
	cleanupUserID := embyapi.UserID("")
	if a.CleanupUser != "" {
		var ok bool
		cleanupUserID, ok = api.GetUserID(a.CleanupUser)
		lo.Must(cleanupUserID, ok, "embyapi: --cleanup-user not found")
	}
	ctx.Bind(cleanupUserID)
}

func mkLogger(lvl int, file string, truncate bool) (wlog.Logger, *os.File) {
	l := wlog.Nfo
	if lvl > 0 {
		l = wlog.Dbg
	} else if lvl == -1 {
		l = wlog.Wrn
	} else if lvl == -2 {
		l = wlog.Err
	} else if lvl < -2 {
		l = wlog.Ftl
	}

	var toClose *os.File
	writer := os.Stdout
	if file != "" {
		flags := os.O_CREATE | os.O_RDWR
		if !truncate {
			flags |= os.O_APPEND
		} else {
			flags |= os.O_TRUNC
		}
		writer = lo.Must(os.OpenFile(file, flags, 0600))
		toClose = writer
	}

	return wlog.New(writer, l, false), toClose
}
