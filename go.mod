module gitlab.com/ddb_db/embysweeper

go 1.22.2

require (
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/oapi-codegen/runtime v1.1.1 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
)

require (
	github.com/alecthomas/kong v0.9.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/deepmap/oapi-codegen v1.16.2
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/samber/lo v1.39.0
	github.com/vargspjut/wlog v1.0.11
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
