# embysweeper

An Emby tool to sweep for and delete media items automatically based on a set of defined rules.

## Version 3

Version 3 is a complete rewrite of this tool.  The key changes in this release:

* Code rewritten in Go; you no longer need Java installed on your system
* Items can be ignored by series tag in addition to series favourite status
* New command: `serieskeep`
  * Ensure only a [set number of episodes](https://gitlab.com/ddb_db/embysweeper/-/wikis/Examples/Ignore-Tagged-Series) of a series are kept, deleting the older ones
* New command: `batch`
  * Run [multiple command lines](https://gitlab.com/ddb_db/embysweeper/-/wikis/Examples/Batch-Processing) on a single run from an execution directory
* Output can be logged to a file
* Commands now accept both include and exclude libraries

### Missing Features

The audit command from older versions is currently not available in v3.  A future release will
attempt to bring this feature back, but it needs a rethink under the new codebase.  Track the progress of this feature [here](https://gitlab.com/ddb_db/embysweeper/-/issues/1).

### Roadmap

* ~Explore idea of integrating at least the `serieskeep` command as plugin directly within Emby~
  * I've explored the requirements, I won't be heading down this road.
* Fix reported bugs

It was ~4 years between 2.0 and 3.0, I expect it'll be at least that long before I work on this again. :)  Until Emby 4.8 introduced API breaking changes that broke embysweeper, this tool was happily running in the background doing its thing for me.  Honestly, I had forgotten about it until it stopped working correctly after I upgraded to Emby 4.8.

Hopefully I've done enough here in the last few weeks that I won't have to touch things again for years to come!

But if you do find a bug and [report it](https://gitlab.com/ddb_db/embysweeper/-/issues), I will try to fix it.  This tool is important, it runs daily against my Emby installation.  Given that its job is to delete media, I do want to make sure it's doing that properly.

## Older Versions

Older versions of embysweeper can be found at the old [Github project](https://github.com/Slugger/embysweeper)

## Installation

Starting with v3, embysweeper is a single executable file.  Binaries for Windows/amd64 and Linux/amd64 will be
built and released on the project site.  The tool can be built for any OS/platform combo that supports Go.
Create an issue ticket if you would like another binary built.

Download the latest release from the project [Releases](https://gitlab.com/ddb_db/embysweeper/-/releases) area.

### No More Docker Images

In older versions, I also created docker images.  I did this because of the Java requirement.  Now that there is no longer any external dependencies, I have stopped building the docker images.  Still want/need the docker?  Open a ticket and let me know. If there's enough interest, I might be able to find some time to do it.

### Virus False Positive

Some virus scanners, especially on Windows, might incorrectly detect that the embysweeper program contains a virus.
If the virus detected is **`Wacatac.B!ml`** then it is almost certainly a false positive that can be ignored.

To be sure, always download from the [project releases area](https://gitlab.com/ddb_db/embysweeper/-/releases) and
compare the sha256 checksum of the **zip/tgz** against the value posted in the release section.

False positives with Go binaries is a known issue on Windows.  See [this FAQ](https://tip.golang.org/doc/faq#virus)
from the Go project for more details.

## Usage

Once you download the executable, run it with just the `-h` flag.  It will show info about all available commands.

`embysweeper -h`

Then run it again with a specific command to get help for that command.

`embysweeper delete -h`

Between the help output and the [various usage examples](https://gitlab.com/ddb_db/embysweeper/-/wikis/Examples), you should be able to build the command needed to manage your Emby media with this tool.

## Examples

Visit the [wiki](https://gitlab.com/ddb_db/embysweeper/-/wikis/Examples) for various examples on how to achieve common
goals using embysweeper.

## Support

If you need help on how to use embysweeper or how to make it do what you're trying to do, post a message in the [Emby forums](https://emby.media/community/index.php?/topic/82842-emby-sweeper/).

If you find a bug or have a feature request, log a ticket [here](https://gitlab.com/ddb_db/embysweeper/-/issues).