package main

import (
	"os"

	"github.com/samber/lo"
	"gitlab.com/ddb_db/embysweeper/internal/cliexpand"
	"gitlab.com/ddb_db/embysweeper/internal/commands"
)

var Version = "dev/unknown"

func main() {
	commands.Start(lo.Must(cliexpand.Expand(os.Args[1:])), Version, nil)
}
